package com.globant.materialdesigncomponents;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TextInputLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_input_layout);
        initViews();
    }

    private void initViews() {
        TextInputLayout TextInputLayoutError = (TextInputLayout) findViewById(R.id.TextInputLayoutError);
        TextInputLayoutError.setError(getString(R.string.error_message_example));
    }
}
