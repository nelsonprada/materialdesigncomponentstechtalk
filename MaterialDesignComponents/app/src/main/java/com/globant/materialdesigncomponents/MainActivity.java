package com.globant.materialdesigncomponents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {

        findViewById(R.id.floating_action_button_textview).setOnClickListener(this);
        findViewById(R.id.bottom_navigation_textview).setOnClickListener(this);
        findViewById(R.id.toolbar_textview).setOnClickListener(this);
        findViewById(R.id.snackbar_textview).setOnClickListener(this);
        findViewById(R.id.coordinator_layout_textview_toolbar).setOnClickListener(this);
        findViewById(R.id.coordinator_layout_textview_fab_snackbar).setOnClickListener(this);
        findViewById(R.id.coordinator_layout_textview_fab_toolbar).setOnClickListener(this);
        findViewById(R.id.coordinator_layout_textview_custom_behavior).setOnClickListener(this);
        findViewById(R.id.text_input_layout_textview).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floating_action_button_textview:
                startActivity(FloatingActionButtonActivity.class);
                break;
            case R.id.bottom_navigation_textview:
                startActivity(BottomNavigationActivity.class);
                break;
            case R.id.toolbar_textview:
                startActivity(ToolBarActivity.class);
                break;
            case R.id.snackbar_textview:
                startActivity(SnackbarActivity.class);
                break;
            case R.id.coordinator_layout_textview_toolbar:
                startActivity(ScrollToolbarActivity.class);
                break;
            case R.id.coordinator_layout_textview_fab_snackbar:
                startActivity(FabSnackbarActivity.class);
                break;
            case R.id.coordinator_layout_textview_fab_toolbar:
                startActivity(FabToolbarActivity.class);
                break;
            case R.id.coordinator_layout_textview_custom_behavior:
                startActivity(FloatingActionButtonActivity.class);
                break;
            case R.id.text_input_layout_textview:
                startActivity(TextInputLayoutActivity.class);
                break;
            default:
                break;
        }
    }

    private void startActivity(Class activity){
        Intent intent = new Intent(MainActivity.this, activity);
        startActivity(intent);
    }
}
