package com.globant.materialdesigncomponents;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SnackbarActivity extends AppCompatActivity implements View.OnClickListener {

    private Button snackbarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snackbar);
        initViews();
    }

    private void initViews() {
        snackbarButton = (Button) findViewById(R.id.snackbarButton);
        snackbarButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.snackbarButton:
                Snackbar.make(findViewById(R.id.rootLayout), R.string.snackbar_message, Snackbar.LENGTH_LONG)
                        .setAction("Action", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(snackbarButton.getCurrentTextColor()==Color.BLUE){
                            snackbarButton.setTextColor(Color.YELLOW);
                        }else {
                            snackbarButton.setTextColor(Color.BLUE);
                        }
                    }
                }).show();
                break;
        }
    }
}
